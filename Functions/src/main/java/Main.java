import javax.security.sasl.SaslClient;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {

    // y = x^2 -> f(x) = x^2, y = f(x)
    //y = f(4) -> 4^2 = 16
    public static int calcSumOfArrayRange(int[] a, int from, int to) {
        int sum = 0;

        // i++ -> i = i + 1
        // a += b -> a = a + b
        for (int i = from; i <= to; i++) {
            sum += a[i];
        }

        return sum;
    }

    public static boolean isEven(int number) {
        return number % 2 == 0;
    }

    // написать функцию, которая возвращает массив, заполненный случайными числами
    public static int[] generateRandomArray(int bound, int arrayLength) {
        // объявили переменную, которая умеет работать со случайными числами
        Random random = new Random();
        // создали массив размера arrayLength, заполненный нулями
        int[] result = new int[arrayLength];
        // пробегаем все элементы массива
        for (int i = 0; i < result.length; i++) {
            // кладем в i-ую позицию в массиве новое случайное число в диапазоне [0, bound - 1]
            result[i] = random.nextInt(bound);
        }
        // возвращаем результат
        return result;

    }

    public static void printNumbersInRange(int from, int to) {
        for (int i = from; i <= to; i ++) {
            System.out.println(i);
        }
    }

    public static void main(String[] args) {

        printNumbersInRange(10, 15);

        int[] x = {34, 10, 11, 56, 57, 93, 10, -3, 32, 15};
        int[] y = {34, 10, 11, 56, 57, 93, 10, -3, 32, 15};

        int sum1 = calcSumOfArrayRange(x, 3, 7);

        System.out.println(sum1);

        int sum2 = calcSumOfArrayRange(y, 2, 6);

        System.out.println(sum2);

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        if (isEven(n)) {
            System.out.println("Число четное");
        } else {
            System.out.println("Число нечетное");
        }
        System.out.println("Введите верхнюю границу чисел");
        int myBound = scanner.nextInt();
        System.out.println("Введите количество чисел");
        int myLength = scanner.nextInt();
        int[] a = generateRandomArray(myBound, myLength);

        System.out.println(Arrays.toString(a));
    }
}
