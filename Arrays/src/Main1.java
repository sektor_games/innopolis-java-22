import java.util.Scanner;

public class Main1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // считываем размер массива с консоли
        int length = scanner.nextInt();

        int[] array = new int[length];

        // считывание массива с консоли
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        int sum = 0;
        // подсчет суммы элементов
        for (int i = 0; i < array.length; i++) {
            sum = sum + array[i]; // sum += array[i]
        }

        System.out.println(sum);
        // 4, 3, 2, 1, 0
        // вывод массива в обратном порядке
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }
    }
}