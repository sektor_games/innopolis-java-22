package example03;

public class Worker extends Human {
    public Worker(String firstName, String lastName) {
        super(firstName, lastName);
    }

    public void go() {
        System.out.println("Я хожу только на работу");
    }

    public void build() {
        System.out.println("Строю дом");
    }
}
