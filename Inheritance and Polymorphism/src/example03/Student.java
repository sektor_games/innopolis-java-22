package example03;

public class Student extends Human {

    private double averageMark;

    public Student(String firstName, String lastName, double averageMark) {
        super(firstName, lastName);
        this.averageMark = averageMark;
    }

    public double getAverageMark() {
        return averageMark;
    }

    public void go() {
        System.out.println("А я только учусь, никуда не хожу");
    }
}
