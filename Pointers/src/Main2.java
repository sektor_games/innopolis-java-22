public class Main2 {
    public static void grow(Human h, double value) {
        h.height += value;
    }

    public static void growNumber(int number, int value) {
        number += value;
    }

    public static void main(String[] args) {
        Human marsel = new Human();
        marsel.height = 1.0;

        int n = 10;

        grow(marsel, 1);
        growNumber(n, 10);

        System.out.println(marsel.height);
        System.out.println(n);
    }
}
