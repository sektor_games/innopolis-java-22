package example01;

public class Main {
    public static void main(String[] args) {
        Nokia nokia = new Nokia();
        iPhone iPhone = new iPhone();

        CoverForIPhone coverForIPhone = new CoverForIPhone(iPhone);
        CoverForNokia coverForNokia = new CoverForNokia(nokia);

        iPhone iPhoneFromCover = coverForIPhone.getPhone();
        Nokia nokiaFromCover = coverForNokia.getPhone();

        System.out.println(nokiaFromCover == nokia);
        System.out.println(iPhoneFromCover == iPhone);
    }
}